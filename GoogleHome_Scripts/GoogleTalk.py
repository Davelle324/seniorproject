#This Script allows users to type a message that is converted to MP3
#and played on a Google Home Device *MUST KNOW IP*
import sys
import pychromecast
import os
import os.path
from gtts import gTTS
import time
import hashlib

ip=input("Enter IP of Google Device:\n")
say=input("Enter words to repeat:\n");

#runs a web server on specified folder
os.system("cd ../mp3_cache && start python -m http.server 8000 --bind 10.0.0.148")
#********* retrieve local ip of my rpi3
import socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#test internet
s.connect(("8.8.8.8", 80))
#obtain local IP (for webserver)
local_ip=s.getsockname()[0]

s.close()
#**********************

fname=hashlib.md5(say.encode()).hexdigest()+".mp3"; #create md5 filename for caching

castdevice = pychromecast.Chromecast(ip)
castdevice.wait()
vol_prec=castdevice.status.volume_level
castdevice.set_volume(0.0) #set volume 0 for not hear the BEEEP
#folder mp3 translations are saved to
try:
   os.mkdir(r"C:\Users\davel\Desktop\mp3_cache")
except:
   pass

if not os.path.isfile(r"C:\Users\davel\Desktop\mp3_cache/"+fname):
   tts = gTTS(say,lang='it')
   tts.save(r"C:\Users\davel\Desktop\mp3_cache/"+fname)
mc = castdevice.media_controller
#Play from localip @ Port 8000
mc.play_media("http://"+local_ip+":8000/"+fname, "audio/mp3")
mc.block_until_active()

mc.pause() #prepare audio and pause...

time.sleep(1)
castdevice.set_volume(vol_prec) #setting volume to precedent value
time.sleep(0.2)

mc.play() #play the mp3
#play full audio before closing and deleting file
while not mc.status.player_is_idle:
  time.sleep(0.5)

mc.stop()
#deletes file after playing
os.system(r"del C:\Users\davel\Desktop\mp3_cache\{}".format(fname))
castdevice.quit_app()