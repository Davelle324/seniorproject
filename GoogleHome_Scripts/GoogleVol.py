#This script allows user to remotely change volume of Google Home
#*MUST KNOW IP*
import sys
import pychromecast

ip=input("Enter IP of Google Device:\n")
vol=-1;

try:
   vol=input("Enter Volume:\n"); #from 0 to 100
except:
   pass

castdevice = pychromecast.Chromecast(ip)
castdevice.wait()

if vol==-1 :
   vol_prec=castdevice.status.volume_level
   print(round(vol_prec*100))
else :
   castdevice.set_volume(float(vol)/100)