#include <stdint.h>
#include <string>
static std::string spi_transfer( int fd, std::string v )
{
  struct spi_ioc_transfer xfer[2];
  int status;
  memset(xfer, 0, sizeof xfer);
  std::string ret;
  ret.resize( v.size() );
  
  xfer[0].tx_buf = (unsigned long)v.data();
  xfer[0].rx_buf = (unsigned long)ret.data();
  xfer[0].len = v.size();
  xfer[0].delay_usecs = 200;
  status = ioctl(fd, SPI_IOC_MESSAGE(1), xfer);
  if (status < 0) {
     perror("SPI_IOC_MESSAGE");
     return "";
  }
  return ret;
}
uint8_t read( address_t addr )
{
  uint8_t d = 77;
  stringstream ss;
  ss << INSTREAD << addr;
  int prefixlength = ss.str().length();
  ss.write( (const char*)&d, sizeof(uint8_t));
  std::string data = spi_transfer( m_fd, ss.str() );
  data = data.substr(prefixlength);
  return data[0];
}
void write( address_t addr, uint8_t b )
{
  spi_write( m_fd, tostr(INSTWREN), true );
  {
    stringstream ss;
    ss << INSTWRITE << addr << flush;
    ss.write( (const char*)&b, sizeof(uint8_t));
    spi_write( m_fd, ss.str(), true );
  }
  spi_write( m_fd, tostr(INSTWRDI), true );
}
std::string tostr( instruction_t v )
{
  std::stringstream ss;
  ss << v;
  return ss.str();
}
static void spi_write( int fd, std::string v )
{
 write( fd, v.data(), v.size() );
}
int main( int argc, char** argv )
{
  EEPROMTest obj( argv[1] );
  address_t addr(305);
  for( uint8_t loopcount = 0; ; loopcount++ )
  {
    int d = (int)obj.read( addr );
    cerr << "MAIN: read data:"  << hex << d << endl;
    cerr << "MAIN: write data:" << hex << (int)loopcount << endl;
    obj.write( addr, loopcount );
    sleep( 1 );
  }
  return 0;
}
