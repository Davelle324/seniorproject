 <HTML>
 <TITLE>PHP WEBPAGE</TITLE>
 <H1> Temperature Readings</H1>
<meta http-equiv="refresh" content="8">
<BODY style="background-color:red;color:white">
<input type="submit" id="btn" value="Logout" >
</br>
<?php
  session_start();
   if ( !isset($_SESSION['loggedin']) || $_SESSION['loggedin'] == false) {
      header("Location: index.php");
}
$servername = "localhost";
$username = "username";
$password = "password";
$dbname = "mydatabase";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$sql = "SELECT Time, Temperature FROM data";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo "Time " . $row["Time"]. " - Temperature: " . $row["Temperature"]. " &#176C<br>";
    }
} else {
    echo "No Temperatures Recorded";
}
$conn->close();

?>
</BODY> 
</HTML>
