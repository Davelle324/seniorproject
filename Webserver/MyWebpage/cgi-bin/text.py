"""Code to Send text message to a mobile number through email"""
print ("Content-type:text/html\r\n\r\n")
import smtplib
import cgi

Providers = dict([('sprint', '@messaging.sprintpcs.com'),('verizon', '@vtext.com'),('t-mobile', '@tmomail.net'),('at&t', '@txt.att.net')])
From = "from@address.com"
form=cgi.FieldStorage()

Number =  form.getvalue('Number')
Service =  form.getvalue('Provider')
msg =  form.getvalue('msg')
#Tests if User Input is one of listed Providers
try:
	if Service.lower() in Providers:
		To=Number+Providers[Service.lower()]
		server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
		server.login("PythonTest324", "TestPassword")	#Credentials for Test Gmail
		server.sendmail(
		  From, 
		  To, 
		  msg)
		server.quit()
		print("Message Sent Successfully Sent")
	else:
		print('ERROR324: Unrecognized Entry')
except:
	print("Failed to Send Message, Check Parameters")
""" 
%%%%%%%%%%%%%%%%%%%%%%%%----List of providers----%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 AT&T: number@txt.att.net (SMS), number@mms.att.net 
 T-Mobile: number@tmomail.net (SMS & MMS) 
 Verizon: number@vtext.com (SMS), number@vzwpix.com 
 Sprint: number@messaging.sprintpcs.com (SMS), number@pm.sprint.com 
 XFinity Mobile: number@vtext.com (SMS), number@mypixmessages.com 
 Virgin Mobile: number@vmobl.com (SMS), number@vmpix.com 
 Tracfone: number@mmst5.tracfone.com 
 Metro PCS: number@mymetropcs.com (SMS & MMS) 
 Boost Mobile: number@sms.myboostmobile.com (SMS), number@myboostmobile.com 
 Cricket: number@sms.cricketwireless.net (SMS), number@mms.cricketwireless.net 
 Republic Wireless: number@text.republicwireless.com (SMS)
 """