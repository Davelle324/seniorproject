"""This program parses the home network gateway for
	my families Cell Phone Names and Sends Text Message to thenm
	when User arrives or leaves
	***Needs textmsg.py and Time12hr***
	Save as .pyw to run in background
"""
import urllib.request
from bs4 import BeautifulSoup
from datetime import datetime
import time
from textmsg import SendMessage2
from Time12hr import Time12

'''
[Janji,Mom,Dutch,Davelle]
Janji=="randys-iphone"==index 0
Mom=="galaxy-s9"==index 1
Dutch=="galaxy-s8"==index 2
Davelle=="somerandomnigga"==index 3
'''
#iteration count
#arrays made to iterate through for maitainence
#Used to keep track of current status
curr=[False,False,False,False]
#Family Cell Phone Names (order of all arrays)
names2=["randys-iphone","galaxy-s9","galaxy-s8","somerandomnigga"]
#used to print names (order of all arrays)
nameprint=["Janji","Mom","Dutch","Davelle"]
#Function that returns status of Network Connection
def Here(Status):
	if Status==True:
		return "Connected to Wifi"
	else:
		return "Disconnected from Wifi"


url = "http://10.0.0.1"
# query the website and return the html to the variable "page"
page = urllib.request.urlopen(url)
soup = BeautifulSoup(page, "html.parser")
# Take out the <div> of name and get its value
name_box=soup.findAll(attrs={'headers': 'host-name'})#, attrs={'class': 'column-1'})
names=[]
#put names into array
for x in name_box:
	name = x.text.strip() # strip() is used to remove starting and trailing
	names.append(name.lower())
#determine current status
for y in range(len(names2)):
	if names2[y] in names:
		curr[y]=True
	else:
		curr[y]=False	
#print if people are home or not

print('''
   Last Refreshed : {4}
	Mom is {0}
	Dutch is {1}
	Janji is {2}
	Davelle is {3}
'''.format(Here(curr[1]),Here(curr[2]),Here(curr[0]),Here(curr[3]),Time12())) 
