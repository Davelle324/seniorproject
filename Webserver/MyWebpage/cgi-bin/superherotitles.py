print('''

<html>
	<head>
		<style>
			dd {
				display: list-item;
				list-style-type: disc;
				}
			dt  {
				font-weight: bold;
				font-size:20;
				}
			H2  {
				font-size:35;
				}
		</style>
	</head>
	

	<body>

		<H2>SuperPowers</H2>
		<dl>
			<dt><a href="https://www.netflix.com/search?q=legend&jbv=80066080&jbp=0&jbr=0" target="_blank"> Legends of Tomorrow</a></dt>
			<dd>Time-travelling rogue Rip Hunter has to recruit a rag-tag team of
			heroes and villains to help prevent an apocalypse that could impact 
			not only Earth, but all of time. When heroes alone are not enough - 
			the world needs legends. ... When heroes alone are not enough - the world needs 
			legends. </dd>
			<dt><a href="https://www.netflix.com/title/80117803?source=35" target="_blank">Raising Dion</a></dt>
			<dd>Raising Dion follows the story of a woman who raises her son Dion after
			the death of her husband Mark. The normal dramas of raising a son as a single
			mom are amplified when Dion starts to manifest several magical, superhero-like 
			abilities. </dd>
			<dt><a href="https://www.netflix.com/title/80057918" target="_blank">Lucifer</a></dt>
			<dd>Bored and unhappy as the Lord of Hell, LUCIFER MORNINGSTAR has abandoned his 
			throne and retired to L.A., where he owns Lux, an upscale nightclub. </dd>
		</dl>
		<H2>Comedy</H2>
		<dl>
			<dt><a href="https://www.netflix.com/title/80178724?source=35" target="_blank">Living with Yourself</a></dt>
			<dd>Living with Yourself follows the story of a man who, after undergoing a mysterious
			treatment that promises him the allure of a better life, discovers that he has been 
			replaced by a cloned version of himself. </dd>
			<dt><a href="https://www.netflix.com/title/80244700?source=35" target="_blank">Nobody's Looking</a></dt>
			<dd>Luck isn't luck, however, it's the courteous help of your fellow guardian angels. 
			One new angel can't seem to stick by the rules though </dd>
		</dl>
		<H2>Crime/Gang</H2>
		<dl>
			<dt><a href="https://www.netflix.com/title/80002479" target="_blank">Peaky Blinders</a></dt>
			<dd>Peaky Blinders is a gangster family epic set in Birmingham, England in 1919, several
			months after the end of the First World War in November 1918. The story centres on the 
			Peaky Blinders gang and their ambitious and highly cunning boss Tommy Shelby </dd>
			<dt><a href="https://www.netflix.com/title/80217669" target="_blank">Top Boy</a></dt>
			<dd>Two London drug dealers ply their lucrative trade at a public housing estate in East
			London. A thriving but underground drugs business is being run by Dushane and his friend
			Sully to become the richest men on the block.</dd>
		</dl>
		<H2>Sci-Fi</H2>
		<dl>
			<dt><a href="https://www.netflix.com/title/81026915" target="_blank">Better than Us</a></dt>
			<dd>'Better Than Us' is set in a world where robots assist humans in everything. It follows
			the story of a special robot who can feel emotions and through her eyes, analyses various 
			aspects of humanity. </dd>
			<dt><a href="https://www.netflix.com/title/80074220" target="_blank">3 %</a></dt>
			<dd>A thriller set in a world sharply divided between progress and devastation, where people
			are given the chance to make it to the "better side" but only 3% of the candidates succeed.
			A world divided into progress and devastation. ... But only 3% make it through. </dd>
			<dt><a href="https://www.netflix.com/title/80105699" target="_blank">Travelers</a></dt>
			<dd>Travellers is a Sci Fi TV show with the premise of a time travel as a backdrop. There is
			a team of 5 travellers, who travel from a bleak and very dark future under the guidance or 
			orders of a mysterious "Director". ... Each team consists of a Historian, a Medic/Doctor, two
			soldiers and one leader </dd>
			<dt><a href="https://www.netflix.com/title/80236118?source=35" target="_blank">VWars</a></dt>
			<dd> V Wars follows the story of the physician-scientist Dr. Luther Swann and his best friend
			Michael Fayne as they face the evolving crisis of a deadly outbreak that fractures society into
			opposing factions, potentially escalating to a future war between humans and vampires.</dd>
		</dl>
	</body>
</html>
''')
