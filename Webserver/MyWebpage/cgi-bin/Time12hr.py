from datetime import datetime
import time

'''returns time in 12 hr format'''

def Time12():
	currentDT = datetime.now()
	x=str(currentDT)
	currentDT=x.split()
	y=currentDT[1]
	d = datetime.strptime(y[:5], "%H:%M")
	d=d.strftime("%I:%M %p")

	x=str(time.ctime())
	date=x[:10]
	return('''{} {} '''.format(date,str(d)))
if __name__ == '__main__':
	Time12()